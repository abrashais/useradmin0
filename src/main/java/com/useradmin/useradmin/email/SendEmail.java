
package com.useradmin.useradmin.email;
/*
import org.springframework.stereotype.Component;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;
import javax.mail.Session;
import javax.mail.Transport;


@Service
public class SendEmail {

    public void emailSender(String email, String msg) {
        String recipient = email;
        String sender = "ahshamar@gmail.com";
        String host = "localhost";
        Properties properties = System.getProperties();
        properties.setProperty("mail.smtp.host", host);
        Session session = Session.getDefaultInstance(properties);

        try
        {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(sender));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));

            message.setSubject("This is Subject");
            message.setText("This is a test mail" + msg);

            Transport.send(message);
            System.out.println("Mail successfully sent");
        }
        catch (MessagingException mex)
        {
            mex.printStackTrace();
        }
    }
}
*/

import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.*;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;

@Service
public class SendEmail {

    public SendEmail() {
    }

    public void sendmail(String email, String token) throws AddressException, MessagingException, IOException {
    Properties props = new Properties();
    props.put("mail.smtp.auth", "true");
    props.put("mail.smtp.starttls.enable", "true");
    props.put("mail.smtp.host", "smtp.gmail.com");
    props.put("mail.smtp.port", "587");

    Session session = Session.getInstance(props, new javax.mail.Authenticator() {
        protected PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication("abrar.ismael2022@gmail.com", "0912022654a");
        }
    });
    Message msg = new MimeMessage(session);
    msg.setFrom(new InternetAddress("abrar.ismael2022@gmail.com", false));

    msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
    msg.setSubject("Password reset token");
    msg.setContent(token, "text/html");
    msg.setSentDate(new Date());

    MimeBodyPart messageBodyPart = new MimeBodyPart();
    messageBodyPart.setContent(token, "text/html");

    Multipart multipart = new MimeMultipart();
    multipart.addBodyPart(messageBodyPart);
//    MimeBodyPart attachPart = new MimeBodyPart();
//    attachPart.attachFile("/var/tmp/image19.png");
//    multipart.addBodyPart(attachPart);
    msg.setContent(multipart);
    Transport.send(msg);
}
}





