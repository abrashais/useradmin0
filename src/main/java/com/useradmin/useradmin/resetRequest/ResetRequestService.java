package com.useradmin.useradmin.resetRequest;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Service
public class ResetRequestService {

    @Autowired
    private ResetRequestRepository resetRequestRepository;

    public void saveRequest(ResetRequest request) {
        resetRequestRepository.save(request);
    }

    public boolean validateTimeAndStatus(String token) {
        LocalDateTime time = LocalDateTime.now();
        LocalDateTime exp = resetRequestRepository.findByToken(token).get().getExpiresAt();
        return time.isBefore(exp) && !isTokenUsed(token);
    }

    public boolean isTokenUsed(String token) {
        return resetRequestRepository.findByToken(token).get().isRequestServed();
    }
}
