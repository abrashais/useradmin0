package com.useradmin.useradmin.resetRequest;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class ResetRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String token;
    private LocalDateTime createdAt;
    private LocalDateTime expiresAt;
    private boolean requestServed;

    public ResetRequest(String token,
                        LocalDateTime createdAt,
                        LocalDateTime expiresAt,
                        boolean requestServed) {
        this.token = token;
        this.createdAt = createdAt;
        this.expiresAt = expiresAt;
        this.requestServed = requestServed;
    }
}
