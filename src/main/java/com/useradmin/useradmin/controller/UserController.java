package com.useradmin.useradmin.controller;

import com.useradmin.useradmin.entity.User;
import com.useradmin.useradmin.repository.UserRepository;
import com.useradmin.useradmin.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    UserRepository userRepository;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/register")
    public User registerUser(@RequestBody User user){
        return userService.registerUser(user);
    }

    @GetMapping
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @GetMapping("/login/{email}/{password}")
    public Object login(@PathVariable String email, @PathVariable String password) {
        return userService.getUserByEmailPassword(email, password);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteById(@PathVariable int id) {
        userService.deleteById(id);
    }

    @PostMapping("/requestReset")
    public String handleRequest(@RequestParam("email") String email) throws MessagingException, IOException {
        userService.passwordChangeRequest(email);
        return "A token is sent to your email to ";
    }

    @PutMapping("/changePassword/{email}/{token}")
    public Object changePassword(@PathVariable("email") String email, @PathVariable("token") String token, @RequestParam("password") String password) {
        return userService.resetPassword(email, token, password);
    }
}
