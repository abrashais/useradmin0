package com.useradmin.useradmin.service;

import com.useradmin.useradmin.resetRequest.ResetRequest;
import com.useradmin.useradmin.resetRequest.ResetRequestRepository;
import com.useradmin.useradmin.resetRequest.ResetRequestService;
//import com.useradmin.useradmin.email.EmailService;
import com.useradmin.useradmin.email.SendEmail;
import com.useradmin.useradmin.entity.User;
import com.useradmin.useradmin.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.UUID;

@Service
@AllArgsConstructor
@NoArgsConstructor
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SendEmail sendEmail;

    @Autowired
    private ResetRequestService resetRequestService;

    @Autowired
    ResetRequestRepository resetRequestRepository;

    public User registerUser(User user) {
        boolean userExists = userRepository.findByEmail(user.getEmail())
                .isPresent();

        if(userExists) {
            throw new IllegalStateException("email already taken!");
        } else {
            return userRepository.save(user);
        }
    }

    public Object getUserByEmailPassword(String email, String password) {
        User wantedUser = userRepository.getUserByEmailAndPassword(email, password);
        if(wantedUser != null) {
            return wantedUser;
        }
        return "this user doesn't exist!";
    }

    public Object getUserByName(String name) {
        User wantedUser = userRepository.getUserByName(name);
        if(wantedUser != null) {
            return wantedUser;
        }
        return "User " + name + " could not be found!";
    }

    public void deleteById(int id) {
        userRepository.deleteById(id);
    }

    public void passwordChangeRequest(String email) throws MessagingException, IOException {

        User user = userRepository.findByEmail(email).get();
        boolean emailExists = userRepository.findByEmail(email)
                .isPresent();
        String token = UUID.randomUUID().toString();
        ResetRequest createdRequest = new ResetRequest(token,
                LocalDateTime.now(),
                LocalDateTime.now().plusMinutes(30), false);
        user.getRequests().add(createdRequest);
        resetRequestService.saveRequest(createdRequest);

       // String link = "http://localhost:8080/users/"+email + "/" + token;
        if (emailExists) {
            sendEmail.sendmail(email, token);
       //     sendEmail.sendmail(email, link);
        }
    }

    public String  resetPassword(String email, String token, String password) {
        if(resetRequestService.validateTimeAndStatus(token)) {
            ResetRequest modified = resetRequestRepository.findByToken(token).get();
            modified.setRequestServed(true);
            resetRequestRepository.save(modified);

            User user = userRepository.findByEmail(email).get();
            user.setPassword(password);
            userRepository.save(user);

            return "The password is successfully changed!";
        } else {
            return "Some thing went wrong. The reason could be that the token might has Expired OR Used!";
        }
    }

    public Object getUserByEmail(String email, String token, String password) {
        User user = userRepository.findByEmail(email).get();
        user.setPassword(password);
        userRepository.save(user);
        return user.getPassword();
    }

}
